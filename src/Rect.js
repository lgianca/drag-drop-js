import React, { Component } from 'react'
// import ReactDOM from 'react-dom';

import ResizableRect from 'react-resizable-rotatable-draggable'
import {RangeStepInput} from 'react-range-step-input';
// import img1 from './img1.jpg'
import './Rect.css';

class Rect extends Component {
  constructor(props) {
    super(props)

    this.state = {
        img1_opacity: 1
    }
    // this.p = {
    //   width: 100,
    //   height: 100,
    //   top: 100,
    //   left: 100,
    //   rotateAngle: 0,
    //   img1_opacity: 1,
    //   img1_src: props.img1_src
    // }

    this.origWidth = 100
    this.origHeight = 100

  }


  setImgSrc = (imgInStr, wIn, hIn) => {
    // let im1 = document.getElementById('img1')
    this.setState({img_src: imgInStr, 
                    width: wIn,
                    height: hIn})
    this.origWidth = wIn
    this.origHeight = hIn
    this.render()
  }

  handleResize = (style, isShiftKey, type) => {
    // type is a string and it shows which resize-handler you clicked
    // e.g. if you clicked top-right handler, then type is 'tr'
    let { top, left, width, height } = style
    top = Math.round(top)
    left = Math.round(left)
    width = Math.round(width)
    height = Math.round(height)

    this.props.onLocationUpdated( {
        top:top,
        left:left,
        width:width,
        height:height
      } )
    // this.setState({
    //   top,
    //   left,
    //   width,
    //   height
    // })

    // alert('handleResize')
  }

  handleRotate = (rotateAngleIn) => {
    this.props.onLocationUpdated( {
        rotateAngle:rotateAngleIn
      } )
    // this.setState({
    //   rotateAngle:rotateAngleIn
    // })
  }

  handleDrag = (deltaX, deltaY) => {
    // this.setState({
    //   left: this.state.left + deltaX,
    //   top: this.state.top + deltaY
    // })
    this.props.onLocationUpdated({
        left: this.props.left + deltaX,
        top: this.props.top + deltaY
      })
  }

  render() {
    const {width, top, left, height, rotateAngle,img_src} = this.props
    return (
      <div className="Rect">
        <p align="left">alpha <RangeStepInput 
                onChange={  
                    e => this.setState({ img1_opacity:e.target.value*0.01})
                    // e => console.log(e.target.value*0.01)
                }
                min={0} max={100}
                step={1}
        /></p>
        
        <img src={img_src} alt="" id="img1" className="Mirror" 
                style={{ 
                    width: `${width}px`,
                    height: `${height}px`,
                    top: `${top}px`,
                    left: `${left}px`,
                    transform: `rotate(${rotateAngle}deg)`,
                    opacity: this.state.img1_opacity
                }}
                />

        <ResizableRect
          left={left}
          top={top}
          width={width}
          height={height}
          rotateAngle={rotateAngle}
          // aspectRatio={false}
          // minWidth={10}
          // minHeight={10}
          zoomable='n, w, s, e, nw, ne, se, sw'
          // rotatable={true}
          // onRotateStart={this.handleRotateStart}
          onRotate={this.handleRotate}
          // onRotateEnd={this.handleRotateEnd}
          // onResizeStart={this.handleResizeStart}
          onResize={this.handleResize}
          // onResizeEnd={this.handleUp}
          // onDragStart={this.handleDragStart}
          onDrag={this.handleDrag}
          // onDragEnd={this.handleDragEnd}
        />
            


      </div>
    )
  }
}

export default Rect