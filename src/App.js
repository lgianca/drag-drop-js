/*
 * 
 * Align images React example
 *
 * 
 */ 
import React, { Component } from 'react'
import './App.css';

// import useImageSize from '@use-hooks/image-size';
import reactImageSize from 'react-image-size';
import Rect from './Rect';




class App extends Component {
  constructor() {
    super();
    

    // This binding is necessary to make `this` work in the callback    
    this.handleChange = this.handleChange.bind(this);  
    this.handleRect1Change = this.handleRect1Change.bind(this);
    this.handleRect2Change = this.handleRect2Change.bind(this);
    this.handleInputChangeRect1 = this.handleInputChangeRect1.bind(this);
    this.handleInputChangeRect2 = this.handleInputChangeRect2.bind(this);

    // reference to Rectangle object (not ideal but needed to call methods externally)
    this.rectImgRef1 = React.createRef();
    this.rectImgRef2 = React.createRef();
    // this.tmpImgRef = React.createRef();

    // this.tmpImg = <img src="" ref={this.tmpImgRef} />

    this.state = {img1src: '', 
                  img2src: '', 
                  coordMsg:'', 
                  rectImg1: { width: 100,
                              height: 100,
                              top: 100,
                              left: 100,
                              rotateAngle: 0,
                              img_src: '',
                              origWidth:0,
                              origHeight:0},
                  rectImg2: { width: 100,
                              height: 100,
                              top: 100,
                              left: 100,
                              rotateAngle: 0,
                              img_src: '',
                              origWidth:0,
                              origHeight:0}
                            
                  };
  }  


  handleChange = (e) => {      
      var strFile = URL.createObjectURL(e.target.files[0]);

      reactImageSize(strFile)
      .then(({ width, height }) => {
          if (e.target.id === "file1"){
            // this.rectImgRef1.current.setImgSrc( strFile, width, height );
            const newState = Object.assign(this.state.rectImg1, {img_src: strFile, width: width, height: height, origWidth: width, origHeight: height})
            this.setState({rectImg1: newState})
          }else{
            // this.rectImgRef2.current.setImgSrc( strFile, width, height );
            const newState = Object.assign(this.state.rectImg2, {img_src: strFile,  width: width, height: height, origWidth: width, origHeight: height})
            this.setState({rectImg2: newState})
        }
      }
      )
      .catch((errorMessage) => alert('loading error'));

  };


  // Get relative coordinates
  getCoord = (e) => {      
    // BG image
    var wOrigBG = this.state.rectImg1.origWidth
    var hOrigBG = this.state.rectImg1.origHeight
    var wBG = this.state.rectImg1.width
    var hBG = this.state.rectImg1.height
    
    // FG image
    var wOrigFG = this.state.rectImg2.origWidth
    var hOrigFG = this.state.rectImg2.origHeight
    var wFG = this.state.rectImg2.width
    var hFG = this.state.rectImg2.height


    var tOffset = this.state.rectImg2.top-this.state.rectImg1.top
    var lOffset = this.state.rectImg2.left-this.state.rectImg1.left
    var angleOffset = this.state.rectImg2.rotateAngle-this.state.rectImg1.rotateAngle
    

    var msg = `bgImage (${wOrigBG}x${hOrigBG} orig) (${wBG}x${hBG} new) |
          fgImage (${wOrigFG}x${hOrigFG} orig) (${wFG}x${hFG} new) |
          Offset (fg-bg)  top ${tOffset}, left ${lOffset}, angle ${angleOffset}
    `    
    this.setState({coordMsg: msg})

    this.clearInputFields()

  };

  clearInputFields(){
    var elements = document.getElementsByTagName("input");
    for (var ii=0; ii < elements.length; ii++) {
      if (elements[ii].type === "text") {
        elements[ii].value = "";
      }
    }
  }

  handleRect1Change( infoIn ){
    const newState = Object.assign(this.state.rectImg1, infoIn)

    this.setState( {rectImg1: newState} )

    // alert('handleResize1')

    // this.render()
    this.clearInputFields()

  }

  handleRect2Change( infoIn ){
    const newState = Object.assign(this.state.rectImg2, infoIn)

    this.setState( {rectImg2: newState} )

    // alert('handleResize1')

    // this.render()
    this.clearInputFields()


  }

  handleInputChangeRect1(e, tgtVarStr){
    // alert(e.target.value)
    const newStateInner = {} 
    newStateInner[tgtVarStr] =parseInt( e.target.value )
    const newState = Object.assign(this.state.rectImg1, newStateInner)
    this.setState( {rectImg1: newState} )

  }
  handleInputChangeRect2(e, tgtVarStr){
    const newStateInner = {} 
    newStateInner[tgtVarStr] = parseInt(e.target.value )
    const newState = Object.assign(this.state.rectImg2, newStateInner)
    this.setState( {rectImg2: newState} )
  }

  render() {
    const r1Info = this.state.rectImg1
    this.rectImg1 = <Rect img_src={r1Info.img_src} 
                  width= {r1Info.width}
                  height= {r1Info.height}
                  top= {r1Info.top}
                  left= {r1Info.left}
                  rotateAngle= {r1Info.rotateAngle}
                  onLocationUpdated={this.handleRect1Change}
                  ref={this.rectImgRef1} />

    const r2Info = this.state.rectImg2
    this.rectImg2 = <Rect img_src={r2Info.img_src} 
                  width= {r2Info.width}
                  height= {r2Info.height}
                  top= {r2Info.top}
                  left= {r2Info.left}
                  rotateAngle= {r2Info.rotateAngle}
                  onLocationUpdated={this.handleRect2Change}
                  ref={this.rectImgRef2} />


    const {coordMsg} = this.state

    return (
      <div className="App">
        <header className="App-header">
          
          <p>
            Image alignment
          </p>
        </header>
        <div align="left">
          <label>Background image</label>  <input type="file" onChange={this.handleChange} id="file1" /><br />
          <label>Foreground image</label>  <input type="file" onChange={this.handleChange} id="file2"  /><br />
          <h2></h2>
          <fieldset>
            BG Image - manual input<br />
            <label>Width</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect1(e, 'width')}}   /><br />
            <label>Height</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect1(e, 'height')}}   /><br />
            <label>Top</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect1(e, 'top')}}   /><br />
            <label>Left</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect1(e, 'left')}}   /><br />
            <label>Angle</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect1(e, 'rotateAngle')}}   /><br />
          </fieldset>          
          <fieldset>
            FG Image - manual input<br />
            <label>Width</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect2(e, 'width')}}   /><br />
            <label>Height</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect2(e, 'height')}}   /><br />
            <label>Top</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect2(e, 'top')}}   /><br />
            <label>Left</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect2(e, 'left')}}   /><br />
            <label>Angle</label>  <input type="text"  onChange={(e) => {this.handleInputChangeRect2(e, 'rotateAngle')}}   /><br />
          </fieldset>
          <button onClick={this.getCoord}>Get coordinates</button> <br />
          {coordMsg}
          
        </div>
        {this.rectImg1}
        {this.rectImg2}
      </div>
    );
  }
}

export default App;
